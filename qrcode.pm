#!/usr/bin/perl
# Ikiwiki qrcode plugin encodes things into qrcode and is released
# under the terms of the GPL version 2, or any later version at your
# option.
# Copyright 2012 by Don Armstrong <don@donarmstrong.com>.


package IkiWiki::Plugin::qrcode;

=head1 NAME

qrcode -- embed qr codes in IkiWiki

=head1 SYNOPSIS

qrcode embed qr codes in IkiWIki


[[!qrcode code="http://www.donarmstrong.com"]]


=head1 DESCRIPTION

=head2 Available options

=over

=item code

QR code to generate

=back


=head1 BUGS

None currently known


=cut

use warnings;
use strict;

use Imager::QRCode qw(plot_qrcode);

use IkiWiki '3.00';

use Encode qw(decode);
use Digest::MD5 qw(md5_hex);

my $id = "qrcode";
sub import {
    hook(type => "getsetup", id => $id, call => \&getsetup);
    hook(type => "preprocess", id => $id, call => \&preprocess);
}

my $link = "http://git.donarmstrong.com/?p=ikiwiki_plugins.git;a=blob;f=qrcode.pm;hb=HEAD";
sub getsetup {
    return(plugin => {safe => 1,
		      rebuild => 1,
		      section => "misc",
		      link => $link,
		      description => "qrcode plugin",
		     },
	  );
}

sub code_md5 {
    return(md5_hex(map {decode('utf8',$_)} @_));
}

sub preprocess {
    my %param = @_;

    if (not exists $param{code}
	or not defined $param{code}
	or not length $param{code}) {
	# default to the link to qrcode for the time being
	$param{code} = $link;
    }

    my $image_loc = '';
    if (exists $param{fig}) {
	$param{size} = '4' unless exists $param{width} and defined $param{width};
	$param{margin} = '3' unless exists $param{height} and defined $param{height};
	for (qw(size margin)) {
	    if ($param{$_} !~ /^\d+$/) {
		error("invalid $_; must be an integer: $param{$_}");
	    }
	}
	my $md5 = code_md5($param{code},$param{size},$param{margin});
	$image_loc = "$param{page}/${md5}.png";
	will_render($param{page},$image_loc);
	my $url = qq(\n\n<img class="qrcode" src=").urlto($image_loc,$param{destpage}).qq(" alt=").html_escape($param{code}).qq("/>\n);
	if (-e "$config{destdir}/$image_loc") {
	    return $url;
	} else {
	    # this makes sure that we can write to the file result
	    writefile($image_loc, $config{destdir}, "");
	    my $img = plot_qrcode(decode('utf8',$param{code}),
				  {size => $param{size},
				   margin => $param{margin},
				   mode  => '8-bit'});
	    $img->write(file => "$config{destdir}/$image_loc");
	}
    }
}

1;


__END__








