#!/usr/bin/perl
# Ikiwiki Apache::Gallery plugin
# Don Armstrong <don@donarmstrong.com> 2011/11/26

package IkiWiki::Plugin::agimg;

use warnings;
use strict;

use IkiWiki 3.00;

use URI::Escape qw(uri_escape_utf8);
use HTML::Entities qw(encode_entities);

sub import {
    hook(type => "getsetup", id => "agimg", call => \&getsetup);
    hook(type => "checkconfig", id => "agimg", call => \&checkconfig);
    hook(type => "preprocess", id => "agimg", call => \&preprocess);
}

sub getsetup () {
    return (plugin => {safe => 1,
		       rebuild => undef,
		       section => "widget",
		      },
	    agimgprefix => {type => "string",
			    example => "/gallery/",
			    description => "Base URI of Apache Gallery site",
			    advanced => 0,
			    safe => 0,
			    rebuild => 1,
			   },
	   );
}

sub checkconfig() {
    if (! defined $config{agimgprefix}) {
	$config{agimgprefix} = ""
    }
}


sub preprocess(@) {
    my ($url) = $_[0] =~ /^(.+)$/; # anything is ok
    my $complete_url = $config{agimgprefix}.$url; #encode_entities($config{agimgprefix} . $url);
    return "<a href=\"$complete_url\"><img class=\"agimg\" src=\"${complete_url}?thumbonly\"></a>";
}


1;
